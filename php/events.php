<?php

// This File contains all queries for database
require_once 'php/db.php';


//function: get_all_events
// retrievs all events from db
function get_all_events($user){
	$sql = "SELECT * FROM EVENTS ORDER BY START_DATE DESC;";
	$db = db_conn();
	$result = $db -> query($sql);
	//$_SESSION['all_events'] = $result;
	$db -> close();
	return $result;
}

//function: get_my_events_created
// get Evnets created by $user
function get_my_events_created($user){
	$sql = 'SELECT * FROM EVENTS WHERE CREATOR="'.$user.'"';
	$db = db_conn();
	$result = $db->query($sql);
	$db->close();
	return $result;
}

//function: create_event
//creates Event in DB
function create_event($user,$room,$title, $description,$start,$end, $public_le, $allday){
	$db = db_conn();
	if(!$allday){
		$all_day = "FALSE";
	}else{
		$all_day ="TRUE";
	}
	if(!$public_le){
	$public_level = "FALSE";
	}else{
	$public_level ="TRUE";
	}
	$sql2 = 'INSERT INTO EVENTS(TITLE,DESCRIPTION,START_DATE,END_DATE,CREATED,STATUS,R_ID,CREATOR, PUBLIC, ALLDAY) VALUES ("'.$title.'","'.$description.'","'.$start.'","'.$end.'",CURRENT_TIMESTAMP,0,"'.$room.'","'.$user.'",'.$public_level.','.$all_day.');';
	print_r($sql2);
	$result = $db -> query($sql2);
	$db->close();
}

function create_event_with_attendee($user,$attendees, $room, $title,$description, $start, $end){
	$db = db_conn();
	$sql = 'INSERT INTO EVENTS(TITLE,DESCRIPTION, START_DATE,END_DATE,CREATED,STATUS,R_ID,CREATOR) VALUES("'.$title.'","'.$description.'","'.$start.'","'.$end.'",CURRENT_TIMESTAMP,0,"'.$room.'","'.$user.'")';
	$sql2 = 'SELECT EVENT_ID FROM EVENTS WHERE CREATOR ='.$user.' AND TITLE='.$title.'';
	$result_1 = $db -> query($sql);
	$result_2 = $db -> query($sql2);
	$db -> close();

}


//function: delete_event
//deletes event from db
function delete_event($event_id, $title){
	$db = db_conn();
	$sql = 'DELETE FROM EVENTS WHERE ID='.$event_id.';';
	print_r($sql);
	$result = $db -> query($sql);
	$db -> close();
}


//function: update_event
//updates Event in DB
function update_event($title,$id, $start_date, $end_date, $public_le, $allday){
	$db = db_conn();
	 if(!$allday){
                $all_day = "FALSE";
        }else{
                $all_day ="TRUE";
        }
        if(!$public_le){
        $public_level = "FALSE";
        }else{
        $public_level ="TRUE";
        }
	$sql = 'UPDATE EVENTS SET START_DATE="'.$start_date.'" , END_DATE="'.$end_date.'" , PUBLIC='.$public_level.', ALLDAY='.$all_day.' WHERE ID='.$id.';';
	print_r($sql);
	$result = $db -> query($sql);
	echo($result);
	$db -> close();
}

//function: update_event_extended
//updates event in db with all fields set
function update_event_extended($title,$id, $start_date, $end_date, $desc, $r_id, $user, $public_le, $allday){
        $db = db_conn();
         if(!$allday){
                $all_day = "FALSE";
        }else{
                $all_day ="TRUE";
        }
        if(!$public_le){
        $public_level = "FALSE";
        }else{
        $public_level ="TRUE";
        }
	$sql = 'UPDATE EVENTS SET TITLE="'.$title.'", DESCRIPTION="'.$desc.'", START_DATE="'.$start_date.'" , END_DATE="'.$end_date.'", R_ID='.$r_id.', PUBLIC= '.$public_level.' , ALLDAY= '.$all_day.'  WHERE ID='.$id.';';
        print_r($sql);
        $result = $db -> query($sql);
        echo($result);
        $db -> close();
}

function get_all_rooms_with_events(){
	$sql = "SELECT COMMON_NAME FROM ROOMS INNER JOIN EVENTS ON ROOMS.ROOM_ID = EVENTS.R_ID";
	$db = db_conn();
	$result = $db-> query($sql);
	$db -> close();
	return $result;
}

function get_free_rooms($user,$start,$end){
	$sql = "";
}

//function: get_rooms
// retrievs all rooms from DB
function get_rooms($user){
	$sql = "SELECT * FROM ROOMS;";
        $db = db_conn();
        $result = $db -> query($sql);
        //$_SESSION['all_events'] = $result;
        $db -> close();
        return $result;
}

function test_full_cal($user){
	$sql = "SELECT TITLE,DESCRIPTION,START_DATE, END_DATE FROM EVENTS ORDER BY START_DATE DESC";
	$db = db_conn();
        $result = $db -> query($sql);
        $db -> close();
}

?>
