<?php

@session_start();

// Daten des zu authentifizierenden Benutzers (z.B. aus Formular)

//function: authenticate
// returns if user is in LDAP and if Pass is correct
function authenticate($username, $password){

//function: load_ini_array
//load ini
$ini_array = parse_ini_file("conf/conf.ini", TRUE);



//var: $ldap_admin
//useraccount
$ldap_admin = $ini_array["ldap"]["user"];

//var: $ldap_admin_pass
//SHA encrypted pass
$ldap_admin_pass = $ini_array["ldap"]["password"];;

//var: $ldap_address
// fqdn or IP of LDAP Server
$ldap_address = $ini_array["ldap"]["address"];

//var: $ldap_port
//port of LDAP Server
$ldap_port = $ini_array["ldap"]["port"];


//struct: connect
//connect to ldap
if ($connect = ldap_connect($ldap_address, $ldap_port)) {
   // Verbindung erfolgreich
   ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
   ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);


//struct: bind
// authentification of user
	$bind_dn = $ini_array["ldap"]["bind_dn"];
	$bind = ldap_bind($connect, $bind_dn,$ldap_admin_pass);

//struct: query ldap
// if bind is successful query ldap for user details
if ($bind) {
      // Login erfolgreich
      // Benutzerdaten abfragen
      $dn = $ini_array["ldap"]["search_dn"];
      $person = "$username";
      $fields = "(|(uid=*$person*))";
      $search = ldap_search($connect, $dn, $fields);
      $result = ldap_get_entries($connect, $search);
      $userDN = $result[0]["dn"];
      $_SESSION['ldap'] = $result;
      $ldap_usr_bind = ldap_bind($connect, $userDN, $password);
      if($ldap_usr_bind) {
	echo "userbind workes";
	$_SESSION['user'] = $username;
	$_SESSION['access'] = 2;
	return true;
	}
	else {
		echo "no bind for usr";
	}
      ldap_close($connect);

   } else {
      echo "cannot bind";
      echo $ldap_address;
      echo $ldap_port;
	return false;
}

} else {
	echo "cannot connect";
	return false;
}
}
?>
