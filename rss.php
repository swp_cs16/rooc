<?php

@session_start();
require_once 'db.php';

$xml = new DOMDocument('1.0', 'utf-8');
$xml->formatOutput = true;
    
$rss = $xml->createElement('rss');
$rss->setAttribute('version', '2.0');
$xml->appendChild($rss);
    
$channel = $xml->createElement('channel');
$rss->appendChild($channel); 

// Head des Feeds    
$head = $xml->createElement('title', 'Event Feed');
$channel->appendChild($head);
    
$head = $xml->createElement('description', 'See when events change or are created');
$channel->appendChild($head);
    
$head = $xml->createElement('language', 'de');
$channel->appendChild($head);
    
$head = $xml->createElement('link', '10.202.134.7');
$channel->appendChild($head);
   
// Aktuelle Zeit, falls time() in MESZ ist, muss 1 Stunde abgezogen werden
$head = $xml->createElement('lastBuildDate', date("D, j M Y H:i:s ", time() - 3600).' GMT');
$channel->appendChild($head);
    


$db = db_conn();
$sql = 'SELECT timestamp, title, article, link FROM RSS ORDER BY timestamp DESC';
$result = $db -> query($sql);

//$event_feed = mysqli_fetch_all($result,MYSQLI_ASSOC);
//$result = mysqli_query('SELECT timestamp, title, article, link FROM rss ORDER BY timestamp DESC', $db);
//print_r($result);


while ($rssdata = mysqli_fetch_array($result))
{	
    $item = $xml->createElement('item');
    $channel->appendChild($item);
        
    $data = $xml->createElement('title', utf8_encode($rssdata["title"]));
    $item->appendChild($data);
    
    $data = $xml->createElement('description', utf8_encode($rssdata["article"]));
    $item->appendChild($data);   
        
    $data = $xml->createElement('link', $rssdata["link"]);
    $item->appendChild($data);
    
    $data = $xml->createElement('pubDate', date("D, j M Y H:i:s ", $rssdata["timestamp"]).' GMT');
    $item->appendChild($data);
    
    $data = $xml->createElement('guid', $rssdata["link"]);
    $item->appendChild($data);
}

$xml->save('rss/rss_feed.xml');

Header('Location: http://10.202.134.7/rooc/rss/rss_feed.xml');    
?>



