<?php

//function: db_conn
//connects to db with credentails from ini File
function db_conn(){
$ini_array = parse_ini_file("conf/conf.ini", TRUE);

$dbHost = $ini_array['database']['server'];
$dbUsername = $ini_array['database']['user'];
$dbPassword = $ini_array['database']['password'];
$dbName = $ini_array['database']['dbname'];


//function: connect
//Connect and select the database
$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

if ($db->connect_error) {
    die("Connection failed: " . $db->connect_error);
}
return $db;
}
?>
