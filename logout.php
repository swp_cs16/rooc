<?php

//struct: Session_start
//starts Session to Overwrite previous session
Session_start();

//struct: session_destroy
//destroy Session
Session_destroy();

//struct: header
//redirect to login
header('Location: ' . $_SERVER['HTTP_REFERER']);

?>
