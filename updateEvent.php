<?php
session_start();

require_once("php/db.php");
require_once("php/events.php");


if(isset($_POST['event_id'])){

//var: $title
$title = $_POST['title'];
//var: $start
$start = $_POST['start'];
//var: $end
$end = $_POST['end'];
//var: $e_id
//event id
$e_id = $_POST['event_id'];

$extended = $_POST['extended'];

//var: $allday
$allday = $_POST['allDay'];

//var: $public_le
$public_le = $_POST['public_le'];
$bool = filter_var($extended, FILTER_VALIDATE_BOOLEAN);
$all_day_bool = filter_var($allday, FILTER_VALIDATE_BOOLEAN);
$public_level = filter_var($public_le, FILTER_VALIDATE_BOOLEAN);

//if(isset($_POST['title']) && isset($_POST['start']) && isset($_POST['event_id'])){
$start2 = date('Y-m-d H:i:s', strtotime($start));
$end2  = date('Y-m-d H:i:s', strtotime($end));

if($bool){
	$user = $_POST['creator'];
	$room_id = $_POST['room_id'];
	$description = $_POST['description'];

//function: events.update_event_extended
// updates Event in DB
	update_event_extended($title, $e_id, $start2, $end2, $description, $room_id, $user, $public_level, $all_day_bool);
}else{
	update_event($title, $e_id, $start2, $end2, $public_level, $all_day_bool);
}

unset($_POST['description']);
unset($_POST['title']);
unset($_POST['start']);
unset($_POST['end']);
unset($_POST['event_id']);
unset($_POST['room_id']);
unset($_POST['extended']);
unset($_POST['user']);
unset($_POST['public_le']);
unset($_POST['allDay']);
$_SESSION["rerenderEvents"]  = true;
}

?>
