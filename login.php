<?php
// initialize session
session_start();

include("php/authenticate.php");


//struct: check_if_user_set
// check to see if user is logging out
if(isset($_GET['out'])) {
	// destroy session
	session_unset();
	$_SESSION = array();
	unset($_SESSION['user'],$_SESSION['access']);
	session_destroy();
}


//struct: check_if_submit
// check to see if login form has been submitted
if(isset($_POST['userLogin'])){


  // run information through authenticator
  if(authenticate($_POST['userLogin'],$_POST['userPassword']))
		{
		// authentication passed
			header("Location: index.php");
			die();
		} else {
		// authentication failed
		$error = 1;
		}

}

//struct: show_error
// output error to user
if(isset($error)){
 echo "Login failed: Incorrect user name, password, or rights <br />";
 echo $error;
}

// output logout success
if(isset($_GET['out']))
{
echo "Logout successful";
die();
}
?>

<style>
input {   width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
}

</style>

<form action="login.php" method="post">
	<h1>Login</h1>
	<label for="userLogin"><b>User (UID)</b></label>
	<input type="text" name="userLogin" /><br />
	<label for="userPassword"><b>Password (LDAP)</b></label>
	<input type="password" name="userPassword" />
	<input type="submit" name="submit" value="Submit" />
</form>
