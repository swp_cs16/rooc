CREATE TABLE ROOMS(
	ROOM_ID INT NOT NULL AUTO_INCREMENT,
	COMMON_NAME VARCHAR(30),
	SHORT_NAME VARCHAR(10),
	PRIMARY KEY (ROOM_ID),
	CAPACITY INT
);

CREATE TABLE EVENTS(
	ID INT NOT NULL AUTO_INCREMENT,
	TITLE VARCHAR(30),
	DESCRIPTION TINYTEXT,
	START_DATE DATETIME,
	END_DATE DATETIME,
	CREATED DATETIME,
	STATUS INT,
	PRIMARY KEY (ID),
	R_ID INT NOT NULL,
	FOREIGN KEY (R_ID) REFERENCES ROOMS (ROOM_ID)
);
