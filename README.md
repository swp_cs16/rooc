# Rooc

# WIP

## Setting up the system
### install ubuntu_server 18.04
apt-get update && upgrade <br />
### install LAMP Stack
apt-get install apache2 <br />
apt-get install mariadb-server mariadb-client <br />
mysql_secure_installation<br />
apt-get install php libapache2-mod-php php-mysql<br />
apt-get install phpmyadmin<br />

### create Databasese using sql script
-> setup_db.sql<br />
### set hostname with script
-> conf_dir -> sh script<br />

### install openLDAP
apt-get install slapd ldap-utils<br />
dpkg-reconfigure slapd (no, dns, organisation, adminpass, MDB, No, Yes, No)<br />
ufw allow ldap<br />
apt-get install phpldapadmin<br />
nano /etc/phpldapadmin/config.php<br />
    -> "$servers->setValue('server','name','RoocLDAP')" <br/>
    -> "$servers->setValue('server','base', array('dc=rooc,dc=net'));" <br />
add members and groups to ldap <br />

### configure memberOf Attribute
ldapadd -x -D cn=admin,dc=rooc,dc=net -W -f basedn.ldif<br />
ldapadd -x -D cn=admin,dc=rooc,dc=net -W -f add_user.ldif<br />
sudo ldapadd -Q -Y EXTERNAL -H ldapi:/// -f memberOf_config.ldif<br />
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f refint1.ldif<br />
sudo ldapadd -Q -Y EXTERNAL -H ldapi:/// -f refint2.ldif<br />
ldapadd -x -D cn=admin,dc=rooc,dc=net -W -f add_group.ldif<br />


### apache
copy conf file to /etc/apache2/sites-available<br />

sudo a2ensite rooc.conf<br />

### final
copy the rest to /var/www/rooc<br />