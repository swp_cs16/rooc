<?php

require_once("php/events.php");
require_once("php/db.php");

// Struct: session_start()
//let php care about Session stuff
session_start();
// Function: get_string_between
// returns substring between two strings
function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}


//function: redirect
// redirects to login if no user is set in session
if(!isset($_SESSION['user'])) {
        // user is not logged in, redirect to login.php
        header("Location: login.php");
        die();
}

//Function: load_events
//get Events for user and present for event Source (fullcalendar)
if(isset($_SESSION["user"])){
	//var_dump($_SESSION["ldap"]);
	require_once('php/loadEvents.php');
	$resp = loadEvents($_SESSION['user']);
}


$usergroup;


//fun ction: get_membership
//get membership of groups from LDAP 
if(isset($_SESSION['user'])){
	require('php/ldap_queries.php');
	$ldapStuff = getAttr();
	//var_dump($ldapStuff);
	$helper = 0;
	foreach($ldapStuff as $item){
		//var_dump($item);
		if(!is_array($item)){
			//var_dump($item);
			$group = get_string_between($item, "cn=", ",");
			$helper++;
			//var_dump($group);
		}else{
			foreach($item as $lowerItem){
				//var_dump($lowerItem);
				$member = get_string_between($lowerItem, "uid=", ",");
				//var_dump($member);
				if($member == $_SESSION['user']){
					if($group == "allpeople"){
					}else{
							$usergroup = $group;
					}
				}
			}
		}
	}
}


?>

<!DOCTYPE html>
<html>
<head>

<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


<meta charset='utf-8' />
<link href='fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link href='css/modal.css' rel='stylesheet' />
<link href='css/modalChange.css' rel='stylesheet' />
<script src='lib/moment.min.js'></script>
<script src='lib/jquery.min.js'></script>
<script src='fullcalendar/fullcalendar.min.js'></script>
<script src='js/popper.min.js'></script>
<script src='js/bootstrap.min.js'></script>
<script src='locale/de.js'></script>
<script src='js/functions.js'></script>

<script>


$(document).ready(function() {

//var: userGroup
//userGroup of current user
var userGroup = "<?php echo $usergroup ?>";

//var: currentUser
//current user
var current_user = "<?php echo $_SESSION['user'] ?>";


//var: welcome
//represents the welcome banner
var welcome = document.getElementById("welcome_banner");

//var: head
// label for welcome Banner
var head = document.createElement("LABEL");
head.append("Welcome " + current_user + "\n You belong to " + userGroup);
head.id = "id_welcome_label";
welcome.appendChild(head);

//var: roomList
//roomList fetched by ajax Request
var roomList = "";                                         
$.ajax({                                                   
    type: "GET",                                           
    url: "roomList.php",                                   
    data: 'get_roomlist',                                  
    dataType: 'json',                                      
    success: function(response){                           
        roomList = response;                               
        console.log(roomList);                             
        cont = true;                                       
    },                                                     
        error: function(jqXHR, textStatus, errorThrown) {  
                console.log(errorThrown);                  
                alert("smth went wrong");                  
                alert(errorThrown);                        
        }                                                  
});                                                        

console.log(roomList);
//Function: filterEvents
//filter events by room selection                         
function filterEvents(room_l){                            
        var eventarray = new Array();                     
        for(var u = 0; u < allEvents.length; ++u){        
                console.log(allEvents[u]);                
                if(room_l.includes(allEvents[u].room_id)){
                        eventarray.push(allEvents[u]);    
                }                                         
        }                                                 
                                                          
        console.log(eventarray);                          
        return eventarray;                                
}                                                         
    

//Function: del_event
//delete an event by button click
function del_event(event){
$('#calendar').fullCalendar('removeEvents', event.id);
$('#calendar').fullCalendar('refetchEvents');

                deleteData = {
                        id: event.id,
                        title: event.title,
                        creator: event.creator
                };
                $.ajax({
                    url: "deleteEvent.php",
                    data: deleteData,
                    type: "POST",
                    success: function(responseData, textStatus, jqXHR){
                                                        alert("Deleted");
                                                },
                                        error: function(jqXHR, textStatus, errorThrown){
                                                        console.log(errorThrown);
                                                        }
                                });

}

//Function: confirm_click
//confirm manipulation of db (event added/updated)
function confirm_click(param_creator, param_event_id, param_start, param_end, param_desc, param_title, param_room_id, param_extended, param_target, param_modal, param_public, param_allday, param_event){
	eventData_modal = {
                creator : param_creator,
		event_id: param_event_id,
                start: param_start,
                end: param_end,
                description: param_desc,
                title: param_title,
                room_id: param_room_id,
                extended: param_extended,
		public_le : param_public,
		allDay: param_allday
       };
	if(param_start < today){                       
        	alert("You can't create Events in the past");   
	}else{
        $.ajax({
                url: param_target,                                                                                                                
                data: eventData_modal,                                                                                                            
                type: "POST",                                                                                                                     
                success: function(responseData, textStatus, jqXHR){                                                                               
                                alert("DONE");
                                //$('#calendar').fullCalendar('updateEvent', param_event);
				//console.log("update event");
                                param_modal.style.display = 'none';                                                                               
                           },                                                                                                                     
                error: function(jqXHR, textStatus, errorThrown) {                                                                                 
                                console.log(errorThrown);
				console.log(textStatus);
                        }                                                                                                                         
                });
		$('#calendar').fullCalendar('renderEvent', eventData_modal, true);
		window.location.reload(true);
} 
}

// define variables

//var: roomJson
// JSON representation of roomlist
var roomjson;

//var: rooms
// Array for roomlist
var rooms = new Array();

//var: modalChange
// Modal for changing events
var modalChange = document.getElementById("id_changeModal");

//var: spanChange
// close button for modal
var spanChange = document.getElementById("id_changeModal_close");

//Function: spanChange.onclick
// closes modal on click
spanChange.onclick = function(){
        modalChange.style.display = "none";
}

//var: confChangeButton
//confitm Button for Change Event Modal
var confChangeBtn = document.getElementById("id_confirmChange_btn");
confChangeBtn.innerHTML = "confirm";

//var: changeStart
// Input vor StartDate
var changeStart = document.getElementById("id_changeStart");

//var: changeEnd
// Input for EndDate
var changeEnd = document.getElementById("id_changeEnd");

//var: changeDesc
// Input for Description
var changeDesc = document.getElementById("id_changeDesc");

//var: changeTitle
// Input for Title
var changeTitle = document.getElementById("id_changeTitle");

//var: changeRoom_select
// Div whic holds select List for rooms
var changeRoom_select = document.getElementById('id_div_room');

//var: selectRoomList
// List for Room Selection
var selectRoomList =  document.getElementById('roomSelect');

//var: selPublic
//  Checkbox to set Event to public
var selPublic = document.getElementById('id_public');

//var: selAllday
// checkbox to mark as allday event
var selAllday = document.getElementById('id_allday');

//var: selParticipants
// output to show participants 
var selPaticipants = document.getElementById('id_parti');

//About: addParticipants
// not implemented yet

//var: allEvents
// Array which holds all events on client side
var allEvents = new Array();

//var: eventSources
// the source for fetching events as JSON Feed
var eventSources = <?php echo json_encode($resp) ?>;

//var: btn_do
// INPUT for selecting events by room
var btn_do = document.createElement('INPUT');
                btn_do.type = "button";

//var: btn_roomplan
// button to show available rooms
var btn_roomPlan = document.getElementById("btn_roomplan");

//var: roomPlanDiv
// div for Room Checkboxes
var roomPlanDiv = document.getElementById("roomplan_checkboxes");

//var: seeDiv
// is the Room List visible
var seeDiv = false;

//var: populated
// has the roomList already entries
var populated = false;


//Function: btn_roomPlan.onclick
// shows roomList to filter events
btn_roomPlan.onclick= function() {
        if(!populated){
	//roomList = getRoomList();
	for (var i = 0; i < Object.keys(roomList).length; i++){
        	var cb = document.createElement("INPUT");
		cb.type = 'checkbox';
		cb.name = Object.values(roomList)[i];
		cb.value = Object.keys(roomList)[i];
		var label = document.createElement('label');
		label.append(Object.values(roomList)[i]);
        	roomPlanDiv.appendChild(cb);
		roomPlanDiv.appendChild(label);
		populated = true;                                                                   
	}
		btn_do.value="show";
		roomPlanDiv.appendChild(btn_do);
		
	}
	if(seeDiv){
		seeDiv = false;
		roomPlanDiv.style.display = "none";
		
	}else{
		seeDiv = true;
		roomPlanDiv.style.display = "grid";
		
	}
}

//Function: btn_do.onclick
// do the event filtering
btn_do.onclick = function() {
	if (rooms != null){ 
		rooms = new Array();
		var c = roomPlanDiv.childNodes;
 		for (var i = 0; i < c.length; i++) {
			if(c[i].nodeName == 'INPUT'){
 	   			if(c[i].checked){
					rooms.push(c[i].value);
				}
			}
		 }
		var roomsJson = JSON.stringify(rooms);
		console.log(roomsJson);
		var newEvents = filterEvents(rooms);
		console.log("new Events " + newEvents.length);
		$('#calendar').fullCalendar("removeEvents");
		$('#calendar').fullCalendar("addEventSource", newEvents);
		console.log("all Events " + allEvents.length);
	}else{
		console.log("error");
	}

}


//get actual Date

//var: today
// actual date
var today = new Date();

//var: day
//this day
var day = today.getDate();

//var: month
//this month
var month = today.getMonth() +1;

//var: year
//this year
var year = today.getFullYear();
if(day <10){
	day = '0'+day;
}
if(month < 10){
  month = '0'+month;
}

today = year + '-' + month + '-' + day;


//struct: fullcalendar
//renders fullcalendar Framework
$('#calendar').fullCalendar({
	  header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      defaultView: 'agendaWeek',
      defaultDate: today,
      navLinks: true,
      selectable: true,
      selectHelper: true,
    //function: fullcalendar.selcet
    // handles selection on fullcalendar
    select: function(start, end, jsEvent, view) {		
		changeStart.value = start.format();
		changeEnd.value = end.format();
		//roomList = getRoomList();
		if(start.format() < today){
			alert("You can't create Events in the past");
			$('#calendar').fullCalendar('unselect');
		}else{
		if (typeof(selectRoomList) != 'undefined' && selectRoomList != null)
                {
                }else{
                        selectRoomList = document.createElement("select");
                        selectRoomList.id = "roomSelect";
                        changeRoom_select.appendChild(selectRoomList);

                for (var i = 0; i < Object.keys(roomList).length; i++){
                        var option = document.createElement("option");
                        option.value = Object.keys(roomList)[i];
                        option.text = roomList[Object.keys(roomList)[i]];
                        selectRoomList.appendChild(option);
                }
                }
		modalChange.style.display = 'block';
                window.onclick = function(event) {
                  if (event.target == modalChange) {
                        modalChange.style.display = "none";
                   }
                }

		confChangeBtn.onclick = function(){
			var room_id_sel = selectRoomList.options[selectRoomList.selectedIndex].value;
			if(start.format() < today){                          
        			alert("You can't create Events in the past");
        			$('#calendar').fullCalendar('unselect');     
			}else{                                               
			if(selAllday.checked){
				var dateStart = new Date(start.format());
				var dateEnd = new Date(end.format());	
				confirm_click(current_user, 0, dateStart.toISOString(), dateEnd.toISOString(), changeDesc.value, changeTitle.value, room_id_sel, true, "addEvent.php", modalChange, selPublic.checked, selAllday.checked);
			
			}else{
				if(is_occupied(changeStart.value, changeEnd.value, room_id_sel, allEvents)){
					alert("the Room is occupied");
				}
				else{
					confirm_click(current_user, 0, changeStart.value, changeEnd.value, changeDesc.value, changeTitle.value, room_id_sel, true, "addEvent.php", modalChange, selPublic.checked, selAllday.checked, jsEvent);
					 //$('#calendar').fullCalendar('renderEvent', eventData_modal, true); 
				}
			}
			}
		}}

                $('#calendar').fullCalendar('refetchEvents');
      	},
	
    //function: fullcalendar.eventResize
    // handles resizing events   
  eventResize: function(event, delta, revertFunc) {
		if(event.allDay){
		eventData3 = {                                                               
                title: event.title,                                          
                start: event.start.format(),                                 
                end: event.end,                                     
                event_id: event.id,
		extended : false,
		allDay: event.allDay,
		public_le: event.public_le                                           
        	};
		}else{
			eventData3 = {              
			title: event.title,         
			start: event.start.format(),
			end: event.end.format(),    
			event_id: event.id,         
			extended : false,           
			allDay: event.allDay,       
			public_le: event.public_le  
		};
		}
		var doit = confirm("Are you sure?");
		if(event.start.format() < today){                          
        		alert("You can't create Events in the past");
		        doit = false;     
		}else{
		}                                               
        	
		if(is_occupied(event.start.format(),event.end.format(),event.room_id, allEvents)){ 
       			 alert("room is occupied");
        		doit = false;
        		revertFunc();
        	}
        	if(doit){               	                                             
                	$.ajax({                                                     
                        	url: "updateEvent.php",                              
                        	data: eventData3,                                    
                        	type: "POST",                                        
                        	success: function(responseData, textStatus, jqXHR){  
                                        alert("Changed");
					console.log(responseData);
					console.log(textStatus);                    
                                },                                           
                       		error: function(jqXHR, textStatus, errorThrown) {    
                                        console.log(errorThrown);            
                                }                                    
                	});
       		 }
                $('#calendar').fullCalendar('refetchEvents');
		},
     
     //function: fullcalendar.eventDrop
    // handles drag and drop events   
	eventDrop: function(event, delta, revertFunc) {
			var startFormat = event.start.format();
			var all = false;
			if(event.end == null && event.allDay == false){
				all = false;
				var endnew = moment(event.start);
				endnew.add(2, 'hours');
				endFormat = endnew.toISOString();
			}
			if(event.end == null && event.allDay == true){
				all = true;
			}
			if(event.end != null){
				endFormat = event.end.format();
			}
			console.log("will be allday " + all);
			if(all){
				eventData2 = {                      
					title: event.title,                 
					start: startFormat,        
					end: event.end,
					event_id: event.id,                 
					extended : false,                   
					allDay: true,               
					public_le: event.public_le          
				};                                  
			}else{                              
				eventData2 = {              
        				title: event.title,         
        				start: startFormat,
       					end: endFormat,    
        				event_id: event.id,         
        				extended : false,           
        				allDay: false,       
        				public_le: event.public_le  
				};
			}
			console.log("Data  "  + eventData2);
			var doit = confirm("Are you sure?");
			if(event.start.format() < today){                       
        			alert("You can't create Events in the past");   
        			doit = false;                                   
			}else{                                                  
			}                                                       
			if(event.allDay){
			
			}else{
				if(is_occupied(startFormat,endFormat,event.room_id, allEvents)){
					alert("room is occupied");
					doit = false;
				}
			}
			if(doit){
				console.log(doit);
				$.ajax({
					url: "updateEvent.php",
					data: eventData2,
					type: "POST",
					success: function(responseData, textStatus, jqXHR){
							alert("Changed");
							$('#calendar').fullCalendar('updateEvent', event);
                                			$('#calendar').fullCalendar('refetchEvents');
							console.log(responseData);
						},
					error: function(jqXHR, textStatus, errorThrown) {
 							console.log(errorThrown);
							console.log(textStatus);
  							}                                       
				});
			}else{
				revertFunc();
                                console.log("revert func");
				}
		
		$('#calendar').fullCalendar('refetchEvents');
		console.log("refetch events");
  		},
	editable: true,
 
     //function: fullcalendar.eventClick
    // handles clicking on events   
	eventClick: function(calEvent, jsEvent, view){
			var start = calEvent.start;
			var end = calEvent.end;
			if(end == null){
				start = start.toString();
				end = "allDay Event";
			}else{
				start = start.toString();
				end = end.toString();
			}
			var modal = document.getElementById('myModal');                
			// Get the <span> element that closes the modal                
			var span = document.getElementsByClassName("close")[0];        
			// When the user clicks the button, open the modal             
			var btnChange = document.getElementById("id_btn_changeEvent");
			btnChange.innerHTML = "Change Event";
			btnChange.onclick = function(){
						modal.style.display = "none";
						changeStart.value = calEvent.start.format();
						if(calEvent.allDay){
						}else{                 
						changeEnd.value = calEvent.end.format();
						}                     
						changeDesc.value = calEvent.description;                            
						changeTitle.value = calEvent.title;
						//roomList = getRoomList();                                 
						if (typeof(selectRoomList) != 'undefined' && selectRoomList != null){
						}else{
							selectRoomList = document.createElement("select");
							selectRoomList.id = "roomSelect";
							changeRoom_select.appendChild(selectRoomList);

							for (var i = 0; i < Object.keys(roomList).length; i++){
								var option = document.createElement("option");
								//console.log(Object.keys(roomList)[i]);
								option.value = Object.keys(roomList)[i];
								//console.log(roomList[Object.keys(roomList)[i]]);
								option.text = roomList[Object.keys(roomList)[i]];
								selectRoomList.appendChild(option);
							}
						}
						modalChange.style.display = "block";
						//spanChange.onclick = function(){
						//	modalChange.style.display = "none";
						//}
						window.onclick = function(event) { 
		  					if (event.target == modalChange) {       
  								modalChange.style.display = "none";    
		 					}
						}
						}
						confChangeBtn.onclick = function(){
							var room_id_sel = selectRoomList.options[selectRoomList.selectedIndex].value;
							confirm_click(current_user, calEvent.id, changeStart.value, changeEnd.value, changeDesc.value, changeTitle.value, room_id_sel, true, "updateEvent.php", modalChange, selPublic.checked ,selAllday.checked, calEvent );
							$('#calendar').fullCalendar('refetchEvents');
						}

	var btnDelete = document.getElementById("id_btn_deleteEvent");
	btnDelete.innerHTML = "delete Event";
	btnDelete.onclick = function(){
		modal.style.display = "none";
		document.getElementById("testOut").innerHTML = "aufruf";	
		del_event(calEvent);
	}


	document.getElementById("modalheader").innerHTML = "TITEL: " + calEvent.title + "     Raum: " + roomList[calEvent.room_id];
	document.getElementById("modalstart").innerHTML = "Start: " + start; 
	document.getElementById("modalend").innerHTML = "Ende: " + end + "<br> <br>" + "allday: " + calEvent.allDay + "<br> <br> Attendees: " + calEvent.attendees;
	document.getElementById("modaldesc").innerHTML = calEvent.description + "<br>" + "public: " + calEvent.public_le;
 	document.getElementById("modalfooter").innerHTML = "Organisator: " + calEvent.creator ;
	if(calEvent.editable == false){
		btnDelete.style.display = 'none';
		btnChange.style.display = 'none';
	}else{
		btnChange.style.display = 'initial';
		btnDelete.style.display = 'initial';
	}
	modal.style.display = "block";
	// When the user clicks on <span> (x), close the modal         
	span.onclick = function() {                                    
	  modal.style.display = "none";                                
	}                                                              
	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	if (event.target == modal) {
	  modal.style.display = "none";
	}
	}
    // change the border color just for fun
    	$(this).css('border-color', 'red');
	},
	
	eventLimit: true, // allow "more" link when too many events
//	events: <?php echo json_encode($resp) ?>,
	events: eventSources,
	eventRender: function(event, element){
		//allEvents = $('#calendar').fullCalendar("clientEvents");
		console.log(event.render);
		if(event.render){
			return true;
		}else{
			return false;
		}
	},

	timeFormat: 'H(:mm)'
    });
	allEvents = $('#calendar').fullCalendar("clientEvents");
	console.log(roomList);
		  });



</script>
<style>
  body {
    margin: 40px 10px;                                              
    padding: 0;                                                     
    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    font-size: 14px;                                                
  }                                                                 
  #calendar {                                                       
    max-width: 1090px;                                               
    margin: 0 auto;                                                 
   float: left;
 }
#welcome_banner {
	border-style:solid;  
	border-color:#2ecec2;
	margin: auto;
	width: 50%;
	padding: 5px;
}

#id_welcome_label {
	margin: auto;
	display: block;
    	text-align: center;
    	line-height: 150%;
    	font-size: .85em;
}
#btn_div {
	grid-template-columns: repeat(3, 1fr);
	grid-gap: 10px;
	border-width:5px;
	border-style:solid;
	border-color:#2ecec2;
	margin: 10px 10%;
	padding: 10px;
	width: auto;
	float: right;
	}
.div_btn {
  display: inline-block;
  border-radius: 4px;
  background-color: #2ecec2;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 14px;
  padding: 10px;
  width: 95%;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
	}

.class_modal_btn{
display: inline-block;  
border-radius: 4px;     
background-color: white;
border: none;           
color: black;           
text-align: center;     
font-size: 14px;        
padding: 10px;          
width: 95%;             
transition: all 0.5s;   
cursor: pointer;        
margin: 5px;            
}


</style>                                                            
</head>                                                             
<body>                                                              
	<div id='welcome_banner'>
	 </div>
	<div id='calendar'></div>
	<span id='btn_span' class='btn_span_class'>
	</span>
	<div id="btn_div">
		<input type="button" id='logout' class='div_btn' onclick="window.location.href='logout.php';" value="LOGOUT"/>
		<Button id='btn_roomplan' class='div_btn'>Room Plan View </Button>
		<div id="roomplan_checkboxes">
		</div>
		<p id='testOut'></p>
	</div>


<!-- The Details Modal -->
<div id="myModal" class="modal">

  <!-- Details Modal -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h2 id="modalheader"></h2>
    </div>
    <div class="modal-body" id="modalBody">
      <p id="modaldesc"></p>
      <p id="modalstart">Some text in the Modal Body</p>
      <p id="modalend">Some other text...</p>
    </div>
    <div class="modal-footer">
      <h3 id="modalfooter"></h3>
      <button class="class_modal_btn" id="id_btn_changeEvent"></button>
      <button class="class_modal_btn" id="id_btn_deleteEvent"></button>
    </div>
  </div>
</div>

<!-- The Change Modal -->                                        
<div id="id_changeModal" class="class_changeModal">                                  
                                                                  
  <!-- Details Modal -->                                          
  <div class="class_changeModal_content">                                     
	<div class="changeModal_header">                                    
		<span id="id_changeModal_close" class="changeModal_close">&times;</span>                          
		<h2 id="id_changeModal_header"></h2>                                  
	</div>                                                        
	<div class="changeModal-body" id="changeModalBody">                       
		<label for="id_changeStart"> startDate: </label>
		<input id="id_changeStart" type="datetime-local"/>
		<label for="id_changeEnd" > endDate: </label>
		<input id="id_changeEnd" type="datetime-local"/>
		<label for="id_changeDesc" > Description: </label>
		<input id="id_changeDesc"/>
		<label for="id_allday" > AllDay: </label>
		<input type="checkbox" id="id_allday" />
		<label for="id_public" > Public: </label>
		<input type="checkbox" id="id_public" />
		<br/>
		<label for="id_changeTitle" > Title: </label>
		<input id="id_changeTitle" />
		<label for="id_div_room" > Room: </label>
		<div id="id_div_room"> </div>
		<label for="id_parti" > Participants: </label>
		<div id="id_parti"> </div>
	</div>                                                        
    	<div class="changeModal-footer">                                    
    	  <h3 id="changeModalfooter"></h3>
	  <div id="id_btn_div" >
                        <button id="id_confirmChange_btn" value="confirm" name="confirm"/>
          </div>                                  
    	</div>                                                        
  </div>                                                          
</div>                                                            



</body>
</html>
