<?php

//struct: Session_start
//recall session variables
session_start();


if(!isset($_SESSION["user"])){
//	die();
}


require_once("php/events.php");


//var: $room_from_db
//all rooms fetched from db
$room_from_db = get_rooms($_SESSION["user"]);

//var: $all_rooms
// extracted Data from mysqli result
$all_rooms = mysqli_fetch_all($room_from_db,MYSQLI_ASSOC);

//var: $roomList
//full roomlist
$room_list = array();
foreach($all_rooms as $single_room){
	$room_list[$single_room["ROOM_ID"]]=$single_room["COMMON_NAME"];
}

//struct: echo json
// echo json encoded list for GET Request
echo json_encode($room_list);

?>
