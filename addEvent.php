<?php
/*adds event to Database*/
/* recall to session */
session_start();

//require db conf and db queries
require_once("php/db.php");
require_once("php/events.php");

//var: creator
// creator of the event
$creator = $_POST['creator'];

//var: title
// event title
$title = $_POST['title'];

//var: start
// startdate of event
$start = $_POST['start'];

//var: end
//enddate
$end = $_POST['end'];

//var: r_id
//room id of event
$r_id = $_POST['room_id'];

//var: desc
//description of event
$desc = $_POST['description'];

//var: public_le
//public status of event
$public_le = $_POST['public_le'];

//var: allday
//allday status of event
$allday = $_POST['allDay'];
$allday_bool = filter_var($allday, FILTER_VALIDATE_BOOLEAN);
$public_le_bool = filter_var($public_le, FILTER_VALIDATE_BOOLEAN);

//if it is at least a start Date given, perform sql query
if(isset($_POST['start'])){
$start2 = date('Y-m-d H:i:s', strtotime($start));
$end2  = date('Y-m-d H:i:s', strtotime($end));
var_dump($_POST);

//function: events.create_event
//adds event to db
create_event($creator, $r_id, $title, $desc ,$start2, $end2, $public_le_bool, $allday_bool); 

unset($_POST['title']);
unset($_POST['start']);
unset($_POST['end']);
unset($_POST['creator']);
unset($_POST['room_id']);
unset($_POST['description']);
unset($_POST['public_le']);
unset($_POST['allDay']);
}


?>
