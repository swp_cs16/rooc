<?php

//function: loadEvents
//loads the events from Database and presents them as a JSON Feed for fullcalendar
function loadEvents($user){

$events = get_all_events($user);
$all_events = mysqli_fetch_all($events,MYSQLI_ASSOC);
$row   = sizeof($all_events);
$i = 0;
while($i <= $row-1){
        $start = date("Y-m-d H:i:s", strtotime($all_events[$i]["START_DATE"]));
        $now = date("Y-m-d H:i:s");
        $dStart = strtotime($start);
        $dEnd = strtotime($now);
        $dDiff = $dStart - $dEnd;
        $diff = date($dDiff/60/60/24);
        if($diff < 0){
                $color = "#909090";
        }elseif (0 < $diff && $diff < 1 ){
                $color = "#FF0000";
        }
        else{
                $color = "#90";
        }
        $end = date("Y-m-d H:i:s", strtotime($all_events[$i]["END_DATE"]));
        $title = $all_events[$i]["TITLE"];
        $room_id = $all_events[$i]["R_ID"];
        $creator = $all_events[$i]["CREATOR"];
        if($creator == $_SESSION['user']){
                $canEdit = true;
        }else{
                $canEdit = false;
        }
        $desc = $all_events[$i]["DESCRIPTION"];
        $id = $all_events[$i]["ID"];
        $allday = $all_events[$i]["ALLDAY"];
        $allday_bool = filter_var($allday, FILTER_VALIDATE_BOOLEAN);
        $public_le = $all_events[$i]["PUBLIC"];
        $public_bool = filter_var($public_le, FILTER_VALIDATE_BOOLEAN);
        $attendees = $all_events[$i]["ATTENDEE"];

	if($allday){
		        $resp[$start . '_' . $id . '_' . $i] = array(
                        'id'    => $id,
                        'title' => $title,
                        'start' => $start,
                        //'end' => $end,
                        'description' => $desc,
                        'room_id' => $room_id,
                        'creator' => $creator,
                        'color' => $color,
                        'public_le' => $public_bool,
                        'allDay'=> $allday_bool,
                        'editable' => $canEdit,
                        'attendees' => $attendees,
                        'displayEventTime' => true,
                	'render' => true,
		);
	}else{
			$resp[$start . '_' . $id . '_' . $i] = array(
                        'id'    => $id,
                        'title' => $title,
                        'start' => $start,
                        'end' => $end,
                        'description' => $desc,
                        'room_id' => $room_id,
                        'creator' => $creator,
                        'color' => $color,
                        'public_le' => $public_bool,
                        'allDay'=> $allday_bool,
			'editable' => $canEdit,
                        'attendees' => $attendees,
                        'displayEventTime' => true,
			'render' => true,
                                );
                }
                $i++;
        }
        $resp = array_values($resp);
	return $resp;
}

?>
